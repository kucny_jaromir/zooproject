//
// Created by kucny on 16/11/2019.
//

#include "Game.h"

int Game::getM_money() const {
    return m_money;
}

int Game::spend_money(int money) {
    if (money > getM_money()){
        std::cout << "You can´t!" << endl;
        return 1;
    }
    else{
        Game::m_money -= money;
        return 0;
    }
}
void Game::earn_money(int money) {
    Game::m_money += money;
}

Storage *Game::getM_storage() const {
    return m_storage;
}

Game::Game(int money){
    m_money = money;
    m_storage = new Storage();
}

Game::Game(int money, Storage *storage) {
    m_money = money;
    m_storage = storage;

}

void Game::play_round(){
    /**
     * V pripade, ze hodime osmicku nebo devitku tak nam konci kolo...
     */
    Printer::printMenu(getM_money());
    int a = Utils::get_correct_numeric_input(1,9);
    while(a!=9){
        switch(a) {
            case 1:
                printer();
                break;

            case 2:
                buyer();
                break;

            case 3:
                seller();
                break;

            case 4:
                Printer::printNotImplemented();
                break;

            case 5:
                Printer::printNotImplemented();
                break;

            case 6:
                Printer::printNotImplemented();
                break;

            case 7:
                Printer::PrintExpenses(calculate_end_turn_expenses());
                break;
            case 8:
                reset_round();
                Printer::printRoundEnd();
                break;

        }
        Printer::printMenu(getM_money());
        a = Utils::get_correct_numeric_input(0,9);
    }

}
void Game::reset_round() {
    for(Machine* mach : m_storage->getM_machines()){
        mach->randomize_prize();
        mach->reset_machine();
        int rc = spend_money(mach->getM_amount_owned()*mach->getM_costs_per_round());
        if(rc == 1){
            Printer::gameLost("You could not pay your bills for machines (working fee per round)");
            throw "You lost!";
        };
    }
    for(Product* product : m_storage->getM_products()){
        product->randomize_prize();
    }
    for(Material* mat: m_storage->getM_materials()){
        mat->randomize_prize_of_material();
        int rc = spend_money(m_storage->getM_storing_fee()*mat->getM_material_count());
        if(rc == 1){
            Printer::gameLost("You could not pay your storage fee bills!");
            throw "You lost!";
        };
    }
}

int Game::calculate_end_turn_expenses() {
    int expenses = 0;
    for(Machine* mach : m_storage->getM_machines()){
        expenses += mach->getM_amount_owned()*mach->getM_costs_per_round();
    }
    for(Product* product : m_storage->getM_products()){
        product->randomize_prize();
    }
    for(Material* mat: m_storage->getM_materials()){
        expenses += m_storage->getM_storing_fee()*mat->getM_material_count();
    }
    return expenses;
}


void Game::buy_machine(string machine_name, int amount) {
    for(Machine* machine : getM_storage()->getM_machines()){
        if(machine->getName_of_machine() == machine_name){
            if(machine->getM_buying_cost()*amount > getM_money()){
                cout << "You don´t have money to buy "<< amount << " pieces of " << machine_name << endl;
            }
            else{
                cout << "Congratulation you just bought "<< amount << " pieces of " << machine_name
                << " for "<< machine->getM_buying_cost()*amount << endl;
                spend_money(machine->getM_buying_cost()*amount);
                machine->buy_machine(amount);
            }
            return;
        }
    }
    cout << "Unable to find this Machine!!! You sure???" << endl;
}

void Game::buy_material(string Material_name, int amount ) {
    for(Material* material : getM_storage()->getM_materials()){
        if(material->getM_material_name() == Material_name){
            if(material->getPrize()*amount > getM_money()){
                cout << "You don´t have money to buy "<< amount << " pieces of " << Material_name << endl;
            }
            else{
                cout << "Congratulation you just bought "<< amount << " pieces of " << Material_name
                     << " for "<< material->getPrize()*amount << endl;
                spend_money(material->getPrize()*amount);
                material->buyMaterial(amount);
            }
            return;
        }
    }
    cout << "Unable to find this Material!!! You sure???" << endl;
}

void Game::sell_machine(string machine_name, int amount) {
    for(Machine * machine : getM_storage()->getM_machines()){
        if(machine->getName_of_machine() == machine_name){
            if(machine->getM_amount_owned() < amount){
                cout << "You are trying to sell more pieces than you have! ABORTED!" << endl;
            }
            else{
                machine->sell_machine(amount);
                earn_money( (amount*(machine->getM_buying_cost()/2)));
            }
            return;
        }
    }
    cout << "Unable to find this machine!!! You sure???" << endl;
}

void Game::sell_material(string Material_name, int amount) {
    for(Material* mat : getM_storage()->getM_materials()){
        if(mat->getM_material_name() == Material_name){
            if(mat->getM_material_count() < amount){
                cout << "You are trying to sell more pieces than you have! ABORTED!" << endl;
            }
            else{
                mat->sellMaterial(amount);
                earn_money((amount*(mat->getPrize()/2)));
            }
            return;
        }
    }
    cout << "Unable to find this material!!! You sure???" << endl;
}

void Game::sell_product(string Product_name, int amount) {
    for(Product* prod : getM_storage()->getM_products()){
        if(prod->getM_name_of_product() == Product_name){
            if(prod->getM_amount_of_products() < amount){
                cout << "You are trying to sell more pieces than you have! ABORTED!" << endl;
            }
            else{
                prod->setM_amount_of_products(prod->getM_amount_of_products()-amount);
                earn_money( (amount*(prod->getM_prize())));
            }
            return;
        }
    }
    cout << "Unable to find this Product!!! You sure???" << endl;
}


void Game::printer() {
    Printer::printPrintInfoInfo();
    int input = Utils::get_correct_numeric_input(1,5);
    switch(input){
        case(1):
            Printer::printInfoAboutStorage(getM_storage(), getM_money());
            break;
        case(2):
            Printer::printYourMachines(getM_storage()->getM_machines(),true);
            break;
        case(3):
            Printer::printYourProducts(getM_storage()->getM_products(), true);
            break;
        case(4):
            Printer::printYourMaterials(getM_storage()->getM_materials(), true);
            break;
        case(5):
            cout << "Doing nothing" << endl;
            break;

    }
}

void Game::seller() {
    Printer::printSellerMenu();
    int input = Utils::get_correct_numeric_input(1,4);
    string name;
    int amount = 0;
    switch(input){
        case(1):
            cout << "Please input name of machine you want to sell" << endl;
            cin >> name;
            cout << "Please input number of machines you want to sell - (0-100)" << endl;
            amount = Utils::get_correct_numeric_input(0,100);
            sell_machine(name,amount);
            break;
        case(2):
            cout << "Please input name of material you want to sell" << endl;
            cin >> name;
            cout << "Please input number of material you want to sell - (0-1000)" << endl;
            amount = Utils::get_correct_numeric_input(0,1000);
            sell_material(name,amount);
            break;
        case(3):
            cout << "Please input name of product you want to sell" << endl;
            cin >> name;
            cout << "Please input number of product you want to sell - (0-1000)" << endl;
            amount = Utils::get_correct_numeric_input(0,1000);
            sell_product(name,amount);
            break;
        case(4):
            cout << "Doing nothing" << endl;
            break;

    }
}

void Game::buyer() {
    Printer::printBuyerMenu();
    int input = Utils::get_correct_numeric_input(1,4);
    string name;
    int amount = 0;
    switch(input){
        case(1):
            cout << "Please input name of machine you want to buy" << endl;
            cin >> name;
            cout << "Please input number of machines you want to buy - (0-100)" << endl;
            amount = Utils::get_correct_numeric_input(0,100);
            buy_machine(name,amount);
            break;
        case(2):
            cout << "Please input name of material you want to buy" << endl;
            cin >> name;
            cout << "Please input number of material you want to buy - (0-1000)" << endl;
            amount = Utils::get_correct_numeric_input(0,1000);
            buy_material(name,amount);
            break;
        case(3):
            creator();
            break;
        case(4):
            cout << "Doing nothing" << endl;
            break;

    }
}


void Game::creator() {
    string name;
    string name_of_machine;
    bool found = false;
    int amount = 0;
    cout << "Please input name of product you want to create" << endl;
    cin >> name;
    cout << "Please input number of products you want to create - (0-1000)" << endl;
    amount = Utils::get_correct_numeric_input(0,1000);
    cout << "Now please enter a name of machine with which you want to create this product" << endl;
    cin >> name_of_machine;
    for(Machine* machine : getM_storage()->getM_machines()){
        if(machine->getName_of_machine() == name_of_machine){
            found = true;
        }
    }
    if(!found){
        cout << "Could not find name of machine!";
        return;
    }

    for(Product* product : getM_storage()->getM_products()){
        if(product->getM_name_of_product() == name){
            if(canCreateProduct(product, amount)){
                for(Machine* mach : getM_storage()->getM_machines()){
                    if(name_of_machine == mach->getName_of_machine()){
                        if(mach->getM_can_work_this_round() > product->getM_time_needed_for_building()*amount){
                            createProduct(product, amount,mach);
                        }
                        else{
                            cout << "This Machine is too tired this round to work that long :(" << endl;
                        }
                    }
                }

            }
            else{
                cout << "You don´t have enough materials to create this many products!" << endl;
            }
            return;
        }
    }
    cout << "Could not find the product, are you sure?" << endl;
}

bool Game::canCreateProduct(Product *product, int amount) {

    for(Material * mat : product->getM_materials_needed_for_construction()){
        if(mat->getM_material_name() != mat->getM_material_name()){
            continue;
        }
        int count = Utils::get_number_of_values_in_vector(product->getM_materials_needed_for_construction(), mat);
        if((count*amount) > mat->getM_material_count()){
            return false;
        }

    }
    return true;
}

void Game::createProduct(Product *product, int amount, Machine* machine) {
    vector<string> used_mats;
    used_mats.clear();
    int how_many = 0;
    for(Material * m :product->getM_materials_needed_for_construction()){

        //Find out if already used this material
        bool is_inside = false;
        for(string mat_name : used_mats){
            if(m->getM_material_name() == mat_name){
                is_inside=true;
            }
        }
        // If not used, count many of them are needed for construction and multiply it by amount of items we want to create
        if(not is_inside){
            how_many = Utils::get_number_of_values_in_vector(product->getM_materials_needed_for_construction(), m);
            for(Material* mat : m_storage->getM_materials()){
                if(mat->getM_material_name() == m->getM_material_name()){
                    mat->sellMaterial(how_many*amount);
                }
            }
        }
        used_mats.push_back(m->getM_material_name());
    }
    machine->work(product->getM_time_needed_for_building()*amount);
    for(Product* prod : getM_storage()->getM_products()){
        if(prod->getM_name_of_product() == product->getM_name_of_product()){
            prod->setM_amount_of_products(prod->getM_amount_of_products()+amount);
        }
    }
}

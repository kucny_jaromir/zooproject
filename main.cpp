#include "Game.h"

int main() {
    Machine* machine = new Machine(1000,15000,"MasinaZaHumny");
    Machine* machine1 = new Machine(250,5000,"MasinaVKoueplne");
    Machine* machine2 = new Machine(5000,60000,"MasinaVDilne");
    Material* material = new Material("Drevo", 50);
    Material* material2 = new Material("Zelezo", 50);
    Material* material3 = new Material("Stribro", 150);
    Material* material4 = new Material("Zlato", 100);
    Material* material5 = new Material("Drahokamy", 500);
    vector<Material*> mats_needed_for_const;
    mats_needed_for_const.clear();
    mats_needed_for_const.push_back(material);
    mats_needed_for_const.push_back(material);
    mats_needed_for_const.push_back(material2);
    vector<Material*> mats;
    mats.push_back(material);
    mats.push_back(material2);
    mats.push_back(material3);
    mats.push_back(material4);
    mats.push_back(material5);
    vector<Material*> mats_needed_for_const1;
    mats_needed_for_const1.push_back(material);
    mats_needed_for_const1.push_back(material2);
    vector<Material*> mats_needed_for_const2;
    mats_needed_for_const2.push_back(material);
    mats_needed_for_const2.push_back(material);
    mats_needed_for_const2.push_back(material);
    mats_needed_for_const2.push_back(material);
    mats_needed_for_const2.push_back(material);
    mats_needed_for_const2.push_back(material);
    mats_needed_for_const2.push_back(material2);
    mats_needed_for_const2.push_back(material2);
    vector<Material*> mats_needed_for_const3;
    mats_needed_for_const3.push_back(material);
    mats_needed_for_const3.push_back(material);
    mats_needed_for_const3.push_back(material);
    mats_needed_for_const3.push_back(material2);
    mats_needed_for_const3.push_back(material2);
    mats_needed_for_const3.push_back(material5);

    vector<Material*> mats_needed_for_const4;
    mats_needed_for_const4.push_back(material);
    mats_needed_for_const4.push_back(material);
    mats_needed_for_const4.push_back(material5);

    vector<Material*> mats_needed_for_const5;
    mats_needed_for_const5.push_back(material);
    mats_needed_for_const5.push_back(material4);

    vector<Material*> mats_needed_for_const6;
    mats_needed_for_const6.push_back(material4);
    mats_needed_for_const6.push_back(material4);
    mats_needed_for_const6.push_back(material5);

    vector<Material*> mats_needed_for_const7;
    mats_needed_for_const7.push_back(material3);
    mats_needed_for_const7.push_back(material3);
    mats_needed_for_const7.push_back(material3);
    mats_needed_for_const7.push_back(material3);
    mats_needed_for_const7.push_back(material3);
    mats_needed_for_const7.push_back(material5);
    mats_needed_for_const7.push_back(material5);
    mats_needed_for_const7.push_back(material5);






    Product* product = new Product("Zidle",mats_needed_for_const,20);
    Product* product2 = new Product("Dlato",mats_needed_for_const1,5);
    Product* product3 = new Product("Postel",mats_needed_for_const2,60);
    Product* product4 = new Product("Kreslo",mats_needed_for_const3,30);
    Product* product5 = new Product("Konik",mats_needed_for_const4,25);
    Product* product6 = new Product("Hracka",mats_needed_for_const5,10);
    Product* product7 = new Product("Sperk",mats_needed_for_const6,30);
    Product* product8 = new Product("Diadem",mats_needed_for_const7,150);


    vector<Product*> products;
    products.clear();
    products.push_back(product);
    products.push_back(product2);
    products.push_back(product3);
    products.push_back(product4);
    products.push_back(product5);
    products.push_back(product6);
    products.push_back(product7);
    products.push_back(product8);

    vector<Machine*> machines;
    machines.clear();
    machines.push_back(machine);
    machines.push_back(machine1);
    machines.push_back(machine2);
    Storage* storage = new Storage(mats,products,machines);

    /**
     * TODO: IMPLEMENT DESTRUCTOR FOR GAME! WHICH WILL DESTROY ETH!
     */


    Game* game = new Game(3000000, storage);
    try{
        Printer::printStoryLaneInfo();
        game->play_round();
    }
    catch(string e){
        delete(material);
        delete(material2);
        delete(product);
        delete(machine);
        delete(game);
        return 0;
    }

}
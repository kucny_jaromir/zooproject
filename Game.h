//
// Created by kucny on 16/11/2019.
//

#ifndef PROJECT_GAME_H
#define PROJECT_GAME_H
#include "Classes/Storage.h"
#include "Utility/Utils.h"
#include "Utility/Printer.h"

#include <iostream>


class Game {
private:
    int m_money;
    Storage* m_storage;
public:
    Game(int money);
    Game(int money, Storage* storage);
    void play_round();
    void reset_round();
    int getM_money() const;
    int spend_money(int m_money);
    void earn_money(int money);
    Storage *getM_storage() const;
    void buy_machine(string machine_name, int amount);
    void buy_material(string Material_name, int amount);
    void sell_machine(string machine_name,int amount);
    void sell_material(string Material_name, int amount);
    void sell_product(string Product_name, int amount);
    int calculate_end_turn_expenses();
    void printer();
    void seller();
    void buyer();
    void creator();
    bool canCreateProduct(Product* product, int amount);
    void createProduct(Product* product, int amount, Machine * machine);
};


#endif //PROJECT_GAME_H

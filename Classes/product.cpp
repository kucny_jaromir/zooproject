//
// Created by kucny on 16/11/2019.
//

#include "product.h"

const vector<Material *> &Product::getM_materials_needed_for_construction() const {
    return m_materials_needed_for_construction;
}

int Product::getM_time_needed_for_building() const {
    return m_time_needed_for_building;
}

int Product::getM_amount_of_products() const {
    return m_amount_of_products;
}

const string &Product::getM_name_of_product() const {
    return m_name_of_product;
}

void Product::setM_time_needed_for_building(int m_time_needed_for_building) {
    Product::m_time_needed_for_building = m_time_needed_for_building;
}

void Product::setM_amount_of_products(int m_amount_of_products) {
    Product::m_amount_of_products = m_amount_of_products;
}

Product::Product(string name_of_product, vector<Material *> materials_needed_for_construction,
                 int time_needed_for_construciton) {

    m_name_of_product=name_of_product;
    m_materials_needed_for_construction=materials_needed_for_construction;
    m_time_needed_for_building = time_needed_for_construciton;
    m_amount_of_products = 0;
    generate_prize();
}

void Product::generate_prize() {
    int final_prize = 0;
    for(Material* mat : getM_materials_needed_for_construction()){
        final_prize += mat->getPrize();
    }
    m_prize = final_prize*3;
}


void Product::randomize_prize() {
    int to = (int) (m_prize * 1.1);
    int from = (int) (m_prize * 0.9);
    m_prize = rand() % (to-from+1)+from;
}

int Product::getM_prize() const {
    return m_prize;
}



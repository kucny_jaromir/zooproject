//
// Created by kucny on 15/11/2019.
//

#ifndef PROJECT_MATERIAL_H
#define PROJECT_MATERIAL_H
#endif //PROJECT_MATERIAL_H


#include <random>
#include <string>
using namespace std;


class Material{
private:
    string m_material_name;
    int m_material_count;
    int m_prize;

public:
    Material(string name, int prize);

    void randomize_prize_of_material();
    int generate_prize(int avg);
    const string &getM_material_name() const;

    int getM_material_count() const;

    int getPrize() const;

    void buyMaterial(int amount);
    void sellMaterial(int amount);
};
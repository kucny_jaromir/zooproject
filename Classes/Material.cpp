//
// Created by kucny on 15/11/2019.
//

#include "Material.h"

Material::Material(string name, int prize) {
    m_material_name = name;
    m_prize = prize;
    m_material_count=0;
}

void Material::randomize_prize_of_material() {
    int to = (int) (m_prize * 1.1);
    int from = (int) (m_prize * 0.9);
    m_prize = rand() % (to-from+1)+from;
}


const string &Material::getM_material_name() const {
    return m_material_name;
}

int Material::getM_material_count() const {
    return m_material_count;
}

int Material::getPrize() const {
    return m_prize;
}

void Material::buyMaterial(int amount) {
    m_material_count += amount;
}

void Material::sellMaterial(int amount) {
    m_material_count -= amount;
}



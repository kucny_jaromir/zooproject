//
// Created by kucny on 16/11/2019.
//

#include "Storage.h"



Storage::Storage(vector<Material *> materials, vector<Product *> products, vector<Machine *> machines) {
    m_machines.clear();
    m_products.clear();
    m_materials.clear();
    m_materials = materials;
    m_products = products;
    m_machines = machines;
    m_storing_fee = 5;

}
Storage::Storage() {
    m_machines.clear();
    m_products.clear();
    m_materials.clear();
    m_storing_fee = 5;
}

const vector<Material *> &Storage::getM_materials() const {
    return m_materials;
}


const vector<Product *> &Storage::getM_products() const {
    return m_products;
}

const vector<Machine *> &Storage::getM_machines() const {
    return m_machines;
}



void Storage::addMachine(Machine *Machine) {
    m_machines.push_back(Machine);
}

void Storage::addProducts(Product *product) {
    m_products.push_back(product);
}

void Storage::addMaterial(Material *material) {
    m_materials.push_back(material);
}

int Storage::getM_storing_fee() const {
    return m_storing_fee;
}


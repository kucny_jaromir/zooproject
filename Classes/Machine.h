//
// Created by kucny on 16/11/2019.
//

#ifndef PROJECT_MACHINE_H
#define PROJECT_MACHINE_H

#include <string>

using namespace std;



class Machine {
    int m_can_work_per_round;
    int m_can_work_this_round;
    int m_costs_per_round;
    int m_buying_cost;
    std::string m_name_of_machine;
    int m_amount_owned;
public:
    Machine(int work_per_round, int costs_per_sec, string name);

    int getM_can_work_per_round() const;

    int getM_costs_per_round() const;

    int getM_buying_cost() const;

    const string &getName_of_machine() const;

    int getM_amount_owned() const;

    int generate_prize(int work_per_round, int costs_per_sec);

    void randomize_prize();

    void sell_machine(int amount);
    void buy_machine(int amount);
    void reset_machine();
    void work(int amount);

    int getM_can_work_this_round() const;
};

#endif //PROJECT_MACHINE_H

//
// Created by kucny on 16/11/2019.
//

#ifndef PROJECT_STORAGE_H
#define PROJECT_STORAGE_H
#include <vector>
#include "Machine.h"
#include "product.h"


class Storage {
private:
    vector<Material*> m_materials;
    vector<Product*> m_products;
    vector<Machine*> m_machines;
    int m_storing_fee;
public:
    Storage();
    Storage(vector<Material*> materials, vector<Product*> products, vector<Machine*> machines);
    void addMachine(Machine* Machine);
    void addProducts(Product* product);
    void addMaterial(Material* material);

    const vector<Material *> &getM_materials() const;

    const vector<Product *> &getM_products() const;

    const vector<Machine *> &getM_machines() const;

    int getM_storing_fee() const;
};


#endif //PROJECT_STORAGE_H

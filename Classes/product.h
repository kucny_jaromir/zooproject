//
// Created by kucny on 16/11/2019.
//

#ifndef PROJECT_PRODUCT_H
#define PROJECT_PRODUCT_H

#include "Material.h"

#include <vector>
#include <random>


class Product {
    vector<Material*> m_materials_needed_for_construction;
    int m_time_needed_for_building;
    int m_amount_of_products;
    string m_name_of_product;
    int m_prize;
public:
    Product(string name_of_product, vector<Material*> materials_needed_for_construction,
            int time_needed_for_construciton);

    const vector<Material *> &getM_materials_needed_for_construction() const;

    int getM_time_needed_for_building() const;

    int getM_amount_of_products() const;

    const string &getM_name_of_product() const;

    void setM_time_needed_for_building(int m_time_needed_for_building);

    void setM_amount_of_products(int m_amount_of_products);

    void generate_prize();

    void randomize_prize();

    int getM_prize() const;

};


#endif //PROJECT_PRODUCT_H

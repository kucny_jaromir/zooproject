//
// Created by kucny on 16/11/2019.
//

#include "Machine.h"

int Machine::getM_can_work_per_round() const {
    return m_can_work_per_round;
}

int Machine::getM_costs_per_round() const {
    return m_costs_per_round;
}

int Machine::getM_buying_cost() const {
    return m_buying_cost;
}

const string &Machine::getName_of_machine() const {
    return m_name_of_machine;
}

int Machine::getM_amount_owned() const {
    return m_amount_owned;
}

Machine::Machine(int work_per_round, int costs_per_sec, string name) {
    m_name_of_machine = name;
    m_buying_cost = generate_prize(work_per_round,costs_per_sec);
    m_can_work_per_round = work_per_round;
    m_can_work_this_round = work_per_round*0;
    m_costs_per_round = costs_per_sec;
    m_amount_owned=0;
}

int Machine::generate_prize(int work_per_round, int costs_per_sec) {
    return work_per_round*100+costs_per_sec*10;
}

void Machine::randomize_prize() {
    int to = (int) (m_buying_cost * 1.1);
    int from = (int) (m_buying_cost * 0.9);
    m_buying_cost = rand() % (to-from+1)+from;
}

void Machine::sell_machine(int amount) {
    if(m_can_work_per_round > m_can_work_this_round*amount){
        m_can_work_this_round = 0;
    }
    else{
        m_can_work_per_round-=amount*m_can_work_this_round;

    }
}

void Machine::buy_machine(int amount) {
    m_amount_owned+=amount;
    m_can_work_this_round+=m_can_work_per_round*amount;
}

void Machine::reset_machine() {
    m_can_work_this_round=m_can_work_per_round*getM_amount_owned();
}

void Machine::work(int amount) {
    m_can_work_this_round -= amount;
}

int Machine::getM_can_work_this_round() const {
    return m_can_work_this_round;
}


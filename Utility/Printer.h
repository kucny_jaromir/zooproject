//
// Created by kucny on 16/11/2019.
//

#ifndef PROJECT_PRINTER_H
#define PROJECT_PRINTER_H
#include "../Classes/Storage.h"
#include <iostream>

#include <vector>
using namespace std;

class Printer {
public:
    static void printInfoAboutStorage(const Storage* storage, int money);
    static void printYourMachines(vector<Machine*> machines, bool justmine);
    static void printYourMaterials(vector<Material*> material, bool justmine);
    static void printYourProducts(vector<Product*> products, bool justmine);
    static void printMenu(int money);
    static void printStoryLaneInfo();
    static void gameLost(string cause);
    static void PrintExpenses(int expenses);
    static void printNotImplemented();
    static void printRoundEnd();
    static void printPrintInfoInfo();
    static void printSellerMenu();
    static void printBuyerMenu();
};


#endif //PROJECT_PRINTER_H

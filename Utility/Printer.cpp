//
// Created by kucny on 16/11/2019.
//

#include "Printer.h"
void Printer::printStoryLaneInfo(){
    cout << "------------------------------------------------------------------------------------" << endl;
    cout << "Byl jednou jeden chlapec, a protoze to byl chlap jako hora, rikali mu Mazanek." << endl;
    cout << "Kdyz mu bylo 25 let, zdelil po svem tatinkovi malou zanedbanou roubenku uprostred valasskych kopcu" << endl;
    cout << "A protoze to byl kluk sikovny, rozhodl se ze chaloupku opravi a udela si v ni rezbarskou dilnicku" << endl;
    cout << "Uprostred prestavby nasel pod kamny zakopany hrnec a v nem hromadu zlataku! Cele 3000000, ktere" << endl;
    cout << "pouzil jako kapital a tvym ukolem je pomoct mu zalozit vlastni uspesnou rezbarskou firmu!" << endl;
    cout << "------------------------------------------------------------------------------------" << endl;
}

void Printer::printMenu(int money) {
    cout << "------------------------------------------------------------------------------------" << endl;
    cout << "Printing menu my fellow boss! Press adequate number from list to perform that action" << endl;
    cout << "Your current balance is: " << money << endl;
    cout << "1 - Print information about something!" << endl;
    cout << "2 - Buy/Create something" << endl;
    cout << "3 - Sell something" << endl;
    cout << "4 - Make new Material(currently unsupported)" << endl;
    cout << "5 - Make new Mroduct(currently unsupported)" << endl;
    cout << "6 - Make new Machine(currently unsupported)" << endl;
    cout << "7 - Print expected expenses on end round" << endl;
    cout << "8 - End round" << endl;
    cout << endl << "9 - end game!" << endl;
    cout << "------------------------------------------------------------------------------------" << endl;


}

void Printer::printInfoAboutStorage(const Storage *storage, int Money) {
    cout << "--------------- Printing Info about your Storage! --------" << endl;
    cout << "You current balance: \t" << Money << endl;
    printYourMachines(storage->getM_machines(), true);
    printYourProducts(storage->getM_products(), true);
    printYourMaterials(storage->getM_materials(),true);
    cout << "----------------------------------------------------------" << endl;
}

void Printer::printYourMachines(vector<Machine *> machines, bool justmine) {
    int counter = 0;
    cout << endl << "You have currently available " << machines.size() << " machines." << endl;
    for(Machine* machine : machines){
        cout << "Machine number  " << counter << endl;
        cout << "Name: \t\t" << machine->getName_of_machine() << endl;
        cout << "Costs per round: \t" << machine->getM_costs_per_round() << endl;
        cout << "One machine can work hours per round: \t" << machine->getM_can_work_per_round() << endl;
        cout << "all machines can work hours per this round: \t" << machine->getM_can_work_this_round() << endl;
        if(justmine and machine->getM_amount_owned()>0){
            cout << "You got " << machine->getM_amount_owned() << " machines." << endl;
        }
        else{
            cout << "You got " << machine->getM_amount_owned() << " machines" << endl;
        }
        cout << "Costs: \t\t" << machine->getM_buying_cost() << endl;
        counter++;
    }
    cout << endl;
}

void Printer::printYourMaterials(vector<Material *> material, bool justmine) {
    int counter = 0;
    cout << endl << "You have currently available " << material.size() <<  " materials." << endl;
    for(Material* mat : material){
        cout << "Material number " << counter << endl;
        cout << "Name: \t\t" << mat->getM_material_name() << endl;
        cout << "It costs: \t" << mat->getPrize() << endl;
        if(justmine and mat->getM_material_count() > 0){
            cout << "You got " << mat->getM_material_count() << " pieces." << endl;
        }
        else{
            cout << "You got " << mat->getM_material_count() << " pieces." << endl;
        }
        counter++;
    }
    cout << endl;
}

void Printer::printYourProducts(vector<Product *> products, bool justmine) {
    int counter = 0;
    cout  << endl << "You have currently available " << products.size() <<  " products." << endl;
    for(Product* product : products){
        cout << "Product number " << counter << endl;
        cout << "Name: \t\t" << product->getM_name_of_product() << endl;
        cout << "Time needed for construction: \t" << product->getM_time_needed_for_building() << endl;
        cout << "Materials needed for construction: \t";
        for( Material* mat : product->getM_materials_needed_for_construction()){
            cout << mat->getM_material_name() << ",";
        }
        cout << endl;
        if(justmine and product->getM_amount_of_products()>0){
            cout << "You got " << product->getM_amount_of_products() << " pieces of this product." << endl;
        }
        else{
            cout << "You got " << product->getM_amount_of_products() << " pieces of this product." << endl;

        }
        counter++;
    }
    cout << endl;
}

void Printer::gameLost(string cause) {
    cout << "Unfortunately, you had to pay for " << cause << endl;
    cout << "Your funds went below 0 so therefore this game ends!";
    throw "Game lost!";
}


void Printer::PrintExpenses(int expenses) {
    cout << endl << endl << "_____________________________________________" << endl << endl << endl;
    cout << "Your expneses after this round will be: " << expenses << endl;
    cout << "_____________________________________________" << endl << endl << endl;

}

void Printer::printNotImplemented() {
    cout << endl << endl <<  "_________________________________________________________________________" << endl;
    cout << "I am sorry this function is not yet implemented, please try it next time!" << endl;
    cout << "_________________________________________________________________________" << endl << endl << endl;

}

void Printer::printRoundEnd(){
    cout << endl << endl << "_________________________________________________________________________" << endl;
    cout << "Round was sucessfully ended!!" << endl;
    cout << "_________________________________________________________________________" << endl << endl << endl;
}

void Printer::printPrintInfoInfo() {
    cout << endl << endl << "_________________________________________________________________________" << endl;
    cout << "Print Menu:" << endl;
    cout << "1 - Print info about whole storage" << endl;
    cout << "2 - Print info about Machines" << endl;
    cout << "3 - Print info about Products" << endl;
    cout << "4 - Print info about Materials" << endl;
    cout << "5 - Changed my mind" << endl;
    cout << "_________________________________________________________________________" << endl << endl << endl;
}
void Printer::printSellerMenu(){
    cout << endl << endl << "_________________________________________________________________________" << endl;
    cout << "Seller Menu:" << endl;
    cout << "1 - Sell machine (for half price)" << endl;
    cout << "2 - Sell material (for half price)" << endl;
    cout << "3 - Sell product!" << endl;
    cout << "4 - Changed my mind" << endl;
    cout << "_________________________________________________________________________" << endl << endl << endl;
}
void Printer::printBuyerMenu(){
    cout << endl << endl << "_________________________________________________________________________" << endl;
    cout << "Seller Menu:" << endl;
    cout << "1 - Buy machine!" << endl;
    cout << "2 - Buy material!" << endl;
    cout << "3 - Create product!" << endl;
    cout << "4 - Changed my mind" << endl;
    cout << "_________________________________________________________________________" << endl << endl << endl;
}
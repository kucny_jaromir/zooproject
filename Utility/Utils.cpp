//
// Created by kucny on 16/11/2019.
//

#include "Utils.h"

int Utils::get_correct_numeric_input(int from, int to) {
    int i = -1;
    cout << "Please input integer between " << from << " and " << to << endl;
    cin >> i;
    while( cin.fail() or  i < from or i > to){
        cin.clear();
        cin.ignore(10000, '\n');
        cout << "Please give proper input! It should be number between " << from << " and "<< to << endl;
        cin >> i;
    }
    cout << "Your input is: " << i << endl;
    return i;
}

int Utils::get_number_of_values_in_vector(vector<Material *> mats, Material *material) {
    int counter = 0;
    for(Material* m : mats){
        if(m->getM_material_name() == material->getM_material_name()){
            counter++;
        }
    }
    return counter;
}

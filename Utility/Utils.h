//
// Created by kucny on 16/11/2019.
//

#ifndef PROJECT_UTILS_H
#define PROJECT_UTILS_H

#include <vector>
#include "../Classes/product.h"
#include <iostream>
using namespace std;


class Utils {
public:
    static int get_correct_numeric_input(int from, int to);
    static int get_number_of_values_in_vector(vector<Material*> mats, Material* material);
};


#endif //PROJECT_UTILS_H
